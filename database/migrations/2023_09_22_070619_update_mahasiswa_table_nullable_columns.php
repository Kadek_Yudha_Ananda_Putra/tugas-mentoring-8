<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMahasiswaTableNullableColumns extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            // Kolom-kolom yang boleh menerima nilai NULL
            $table->string('nama')->nullable()->change();
            $table->string('fakultas')->nullable()->change();
            $table->string('prodi')->nullable()->change();
            $table->string('no_telpon')->nullable()->change();
            $table->string('jenis_kelamin')->nullable()->change();
            $table->text('alamat')->nullable()->change();
            $table->date('tanggal_lahir')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            // Kembalikan kolom-kolom ke status sebelumnya (tidak nullable)
            $table->string('nama')->change();
            $table->string('fakultas')->change();
            $table->string('prodi')->change();
            $table->string('no_telpon')->change();
            $table->string('jenis_kelamin')->change();
            $table->text('alamat')->change();
            $table->date('tanggal_lahir')->change();
        });
    }
}

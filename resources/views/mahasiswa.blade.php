<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Import and Export Excel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        body {
            background-color: red;
            padding: 0;
            margin: 0;
        }

        .center-container {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            text-align: center;
        }

        h2 {
            color: #333;
        }

        .btn,
        .form-control-file {
            margin-bottom: 0;
        }
    </style>
</head>

<body>
    <div class="center-container">
        <h2>Export dan Import Data Mahasiswa</h2>
        <hr>

        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('export-mahasiswa') }}" class="btn btn-success mb-3">Export Data Mahasiswa</a>
            </div>
            <hr>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('import-mahasiswa') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <label for="file" class="m-2">Import Data Mahasiswa (Excel)</label>
                    <div class="form-group">
                        <input type="file" class="form-control-file btn btn-success" id="file" name="file">
                        <button type="submit" class="btn btn-primary" id="importButton" disabled>Import</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        // Function to enable or disable the "Import" button based on file input status
        function toggleImportButton() {
            const fileInput = document.getElementById('file');
            const importButton = document.getElementById('importButton');

            if (fileInput.files.length > 0) {
                importButton.removeAttribute('disabled');
            } else {
                importButton.setAttribute('disabled', 'true');
            }
        }

        // Attach an event listener to the file input to call the toggleImportButton function
        document.getElementById('file').addEventListener('change', toggleImportButton);
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</body>

</html>
